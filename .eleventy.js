// const eleventySass = require("@11tyrocks/eleventy-plugin-sass-lightningcss");

module.exports = (config) => {
  config.addPassthroughCopy("./src/styles.css");
  config.addPassthroughCopy("./src/App.js");

  return {
    // markdownTemplateEngine: "njk",
    // dataTemplateEngine: "njk",
    // htmlTemplateEngine: "njk",
    dir: {
      input: "src",
      output: "public",
    },
  };
};
