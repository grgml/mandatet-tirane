const { useState, useEffect, Fragment } = React;

function App() {
  const [parties, setParties] = useState({
    BASHKE: {
      vota: 7538,
    },
    PS: {
      vota: 147267,
    },
    BF: {
      vota: 67999,
    },
    PD: {
      vota: 19827,
    },
    PDIU: {
      vota: 5313,
    },
    NTH: {
      vota: 6993,
    },
    PSD: {
      vota: 12796,
    },
    PR: {
      vota: 4454,
    },
    LZHK: {
      vota: 4759,
    },
  });

  useEffect(() => {
    llogaritDeputetet(parties, 61);
  }, []);

  const [mandate, setMandatet] = useState([]);
  const [perqindje, setPerqindje] = useState(false);

  const handleChange = ({ target: { value: votes } = {} }, party) => {
    if (perqindje) {
      const newParties = { ...parties };
      newParties[party].perqindje = Number.parseInt(votes);
      const mandatet = llogaritDeputetetPerqindje(newParties, 61);
      // setParties(mandatet);
    } else {
      const newParties = { ...parties };
      newParties[party].vota = Number.parseInt(votes);
      const mandatet = llogaritDeputetet(newParties, 61);
      setParties(mandatet);
    }
  };

  const llogaritDeputetet = (votat, nrMandateve = 61) => {
    // ruajme një kopje të objektit votat, për ta perdorur për llogaritje
    var temp = JSON.parse(JSON.stringify(votat));
    // inicializo mandatet
    for (var parti in votat) {
      votat[parti].mandate = 0;
    }

    const mandatet = [];
    // shpërnda mandatet mes partive
    while (nrMandateve > 0) {
      // llogarit se kujt i takon mandati, pra cila parti ka me shumë vota
      var partia = Object.keys(temp).reduce(function (a, b) {
        return temp[a].vota > temp[b].vota ? a : b;
      });
      // shto një mandat partise
      votat[partia].mandate++;
      mandatet.push(`${partia} ${votat[partia].mandate}`);
      // pjesto numrin total të votave të partise me mandatet + 1
      temp[partia].vota = votat[partia].vota / (votat[partia].mandate + 1);
      nrMandateve--;
    }

    // extra mandate
    const votatCopy = JSON.parse(JSON.stringify(votat));
    for (let i = 0; i < 7; i++) {
      var partia = Object.keys(temp).reduce(function (a, b) {
        return temp[a].vota > temp[b].vota ? a : b;
      });

      votatCopy[partia].mandate++;
      mandatet.push(`${partia} `);
      // pjesto numrin total të votave të partise me mandatet + 1
      temp[partia].vota =
        votatCopy[partia].vota / (votatCopy[partia].mandate + 1);
      nrMandateve--;
    }

    // printo numrin e mandateve për cdo parti
    for (var p in votat) {
      console.log(p + ": " + votat[p].mandate + " mandate");
    }

    console.log(votat);
    console.log(mandatet);
    setMandatet(mandatet);

    return votat;
  };

  const llogaritDeputetetPerqindje = (perqindjet, nrMandateve = 61) => {
    console.log(perqindjet);
  };

  return (
    <Fragment>
      {/* <button onClick={() => setPerqindje((p) => !p)}>
        Konverto ne perqindje
      </button> */}
      <section className="box">
        <form className="stack">
          {Object.keys(parties).map((party, i, all) => (
            <Fragment key={i}>
              <div className="switcher">
                <label htmlFor={party}> {party} </label>
                <input
                  onChange={(val) => handleChange(val, party)}
                  type="number"
                  {...(perqindje ? { min: 0, max: 100 } : null)}
                  id={party}
                  step={perqindje ? "0.01" : "50"}
                  value={
                    perqindje ? parties[party].perqindje : parties[party].vota
                  }
                />
                <span className="mandate">
                  {parties[party].mandate} mandat
                  {parties[party].mandate == 1 ? null : "e"}
                </span>
              </div>
              {i < all.length - 1 ? <hr style={{ width: "100%" }} /> : null}
            </Fragment>
          ))}
          {perqindje ? (
            <div className="switcher">
              <label htmlFor={"tjeter"}> Te tjera </label>
              <input
                type="number"
                disabled
                min="0"
                max="100"
                step="0.01"
                id="tjeter"
                value={0}
              />
              {/* <span className="mandate">
                {parties[party].mandate} mandat
                {parties[party].mandate == 1 ? null : "e"}
              </span> */}
            </div>
          ) : null}
        </form>
      </section>
      <br />
      <section className="mandatet">
        {mandate.map((m, i) => {
          if (i > 60)
            return (
              <div key={i} className={`${m} fail`}>
                {i + 1} - {m}
              </div>
            );
          return (
            <div className={m} key={i}>
              {i + 1} - {m}
            </div>
          );
        })}
      </section>
    </Fragment>
  );
}

// Render the React component in the DOM
ReactDOM.render(<App />, document.getElementById("app"));
